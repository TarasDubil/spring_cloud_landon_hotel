package com.dubilok.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Taras Dubil on 3/09/2020.
 */

@Getter
@Setter
@Entity
@Table(name="ROOM")
public class Room {
    @Id
    @Column(name="ROOM_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="NAME")
    private String name;
    @Column(name="ROOM_NUMBER")
    private String roomNumber;
    @Column(name="BED_INFO")
    private String bedInfo;
}
