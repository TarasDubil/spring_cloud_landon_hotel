package com.dubilok.repository;

import com.dubilok.model.Guest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Taras Dubil on 3/09/2020.
 */
@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
    Guest findByEmailAddress(String emailAddress);
}
