package com.dubilok.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Taras Dubil on 3/09/2020.
 */

@Getter
@Setter
public class Reservation {
    private long id;
    private long roomId;
    private long guestId;
    private String reservationDate;
}
