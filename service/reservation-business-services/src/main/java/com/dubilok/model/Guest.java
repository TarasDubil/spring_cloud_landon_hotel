package com.dubilok.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Taras Dubil on 3/09/2020.
 */

@Getter
@Setter
public class Guest {
    private long id;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String address;
    private String country;
    private String state;
    private String phoneNumber;
}
