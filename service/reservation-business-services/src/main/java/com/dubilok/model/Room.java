package com.dubilok.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Taras Dubil on 3/09/2020.
 */

@Getter
@Setter
public class Room {
    private long id;
    private String name;
    private String roomNumber;
    private String bedInfo;
}
