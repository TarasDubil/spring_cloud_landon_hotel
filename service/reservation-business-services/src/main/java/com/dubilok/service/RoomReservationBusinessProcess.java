package com.dubilok.service;

import com.dubilok.client.GuestService;
import com.dubilok.client.ReservationService;
import com.dubilok.model.Guest;
import com.dubilok.model.Reservation;
import com.dubilok.model.RoomReservation;
import com.dubilok.client.RoomService;
import com.dubilok.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Taras Dubil on 3/09/2020.
 */
@Service
public class RoomReservationBusinessProcess {

    private GuestService guestService;
    private RoomService roomService;
    private ReservationService reservationService;

    @Autowired
    public RoomReservationBusinessProcess(GuestService guestService, RoomService roomService, ReservationService reservationService) {
        this.guestService = guestService;
        this.roomService = roomService;
        this.reservationService = reservationService;
    }

    public List<RoomReservation> getRoomReservationsForDate(String dateString) {
        List<Room> rooms = roomService.findAll(null);
        Map<Long, RoomReservation> roomReservationMap = createRoomReservation(rooms);
        List<Reservation> reservations = reservationService.findAll(dateString);
        if (reservations != null) {
            reservations.forEach(reservation -> {
                Guest guest = guestService.findOne(reservation.getGuestId());
                if (guest != null) {
                    RoomReservation roomReservation = roomReservationMap.get(reservation.getRoomId());
                    roomReservation.setDate(reservation.getReservationDate());
                    roomReservation.setFirstName(guest.getFirstName());
                    roomReservation.setLastName(guest.getLastName());
                    roomReservation.setGuestId(guest.getId());
                }
            });
        }
        return new ArrayList<>(roomReservationMap.values());
    }

    private static Map<Long, RoomReservation> createRoomReservation(List<Room> rooms) {
        Map<Long, RoomReservation> roomReservationMap = new HashMap<>();
        rooms.forEach(room -> {
            RoomReservation roomReservation = new RoomReservation();
            roomReservation.setRoomId(room.getId());
            roomReservation.setRoomName(room.getName());
            roomReservation.setRoomNumber(room.getRoomNumber());
            roomReservationMap.put(room.getId(), roomReservation);
        });
        return roomReservationMap;
    }
}
