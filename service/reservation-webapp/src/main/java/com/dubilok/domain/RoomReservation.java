package com.dubilok.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Taras Dubil on 3/09/2020.
 */

@Getter
@Setter
public class RoomReservation {
    private long roomId;
    private long guestId;
    private String roomName;
    private String roomNumber;
    private String firstName;
    private String lastName;
    private String date;
}